import { Ng2VotingPage } from './app.po';

describe('ng2-voting App', function() {
  let page: Ng2VotingPage;

  beforeEach(() => {
    page = new Ng2VotingPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
