var express = require('express');
var path = require('path');
var logger = require('morgan');
var bodyParser = require('body-parser');
var routes = require('./routes/index');
var app = express();

var http = require('http');
var server = http.createServer(app);
var io = require('socket.io').listen(server);
server.listen(8000);
io.set("origins", "*:*");

var candidates = {};
var currentResult = {
  'candidates' : [],
  'min' : 0,
  'max' : 0
};

io.on('connection', function (socket) {
  socket.emit('resultUpdate', currentResult);
  socket.on('vote', function (candidateName) {
    if(candidates.hasOwnProperty(candidateName)) {
      candidates[candidateName]['count']++;
      currentResult['max'] = Math.max(currentResult['max'], candidates[candidateName]['count']);

      currentResult.candidates = [];
      for (var candidate in candidates) {
        currentResult.candidates.push(candidates[candidate]);
      }

      currentResult['min'] = currentResult.candidates.reduce(function (min, candidate) {
        return Math.min(min, candidate.count);
      }, currentResult['max'] );
    }

    socket.emit('resultUpdate',currentResult);
    socket.broadcast.emit('resultUpdate',currentResult);
  });

  socket.on('newCandidate', function (candidateName) {
    if(!candidates.hasOwnProperty(candidateName)) {
      candidates[candidateName] = {candidateName:candidateName , count:0};
      currentResult.candidates.push({candidateName:candidateName , count:0})
      currentResult['min'] = 0;
    }
    socket.emit('resultUpdate',currentResult);
    socket.broadcast.emit('resultUpdate',currentResult);
  });
});

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, '../dist/')));

app.use('/', routes);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

module.exports = app;
