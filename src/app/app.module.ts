import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common';

import { AppComponent } from './app.component';
import { ResultComponent } from './result/result.component';
import { PollRoomComponent } from './poll-room/poll-room.component';
import { CandidateResultComponent } from './result/candidate-result/candidate-result.component';
import {SocketService} from "./shared/socket.service";

@NgModule({
  declarations: [
    AppComponent,
    ResultComponent,
    PollRoomComponent,
    CandidateResultComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [SocketService],
  bootstrap: [AppComponent]
})
export class AppModule { }
