import { Component } from '@angular/core';
import {Result} from "./shared/result";
import {SocketService} from "./shared/socket.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'RBS Cool Polling System!';
  result:Result = new Result();

  constructor(private ss:SocketService) {
    ss.socket.on('resultUpdate', function(data){
      this.result.update(data.candidates, data.min, data.max);
    }.bind(this));
  }
}
