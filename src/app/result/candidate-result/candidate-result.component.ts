import {Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'rbs-candidate-result',
  templateUrl: './candidate-result.component.html',
  styleUrls: ['./candidate-result.component.css']
})
export class CandidateResultComponent implements OnInit {

  @Input() cName:string;
  @Input() lClass:string ='';
  @Input() nCount:number =0;

  constructor() { }

  createRange(length:number){
    return new Array(length);
  }

  ngOnInit() {
  }

}
