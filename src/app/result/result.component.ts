import {Component, OnInit, Input} from '@angular/core';
import {Result} from "../shared/result";
import {CandidateVoteCount} from "../shared/candidate-vote-count";

@Component({
  selector: 'rbs-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css']
})
export class ResultComponent implements OnInit {

  @Input() result:Result;
  summary:string ='';

  constructor() { }

  getClass(count:number){
    return this.result.getClass(count);
  }

  viewSummary(candidateVoteCount:CandidateVoteCount){
    this.summary = this.result.getSummary(candidateVoteCount);
  }

  hideSummary() {
    this.summary ='';
  }

  ngOnInit() {
  }
}
