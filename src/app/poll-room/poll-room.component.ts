import {Component, Input} from '@angular/core';
import {CandidateVoteCount} from "../shared/candidate-vote-count";
import {SocketService} from "../shared/socket.service";

@Component({
  selector: 'rbs-poll-room',
  templateUrl: './poll-room.component.html',
  styleUrls: ['./poll-room.component.css']
})
export class PollRoomComponent {

  @Input() candidates: CandidateVoteCount[] = [];
  private socket;

  constructor(private ss:SocketService) {
    this.socket=ss.socket;
  }

  castVote(candidate: CandidateVoteCount) {
    this.socket.emit('vote', candidate.candidateName);
  }

  onSubmit(f) {
    if (f.value == "") {
      return;
    }

    this.socket.emit('newCandidate', f.value);

    f.value = "";
    f.focus();
  }
}
