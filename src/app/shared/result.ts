import {CandidateVoteCount} from "./candidate-vote-count";

export class Result {
  private _candidates: CandidateVoteCount[];
  private _min: number;
  private _max: number;

  constructor() {
  }

  update(_candidates: CandidateVoteCount[], _min: number, _max: number) {
    this._candidates = _candidates;
    this._min = _min;
    this._max = _max;
  }

  get candidates(): CandidateVoteCount[] {
    return this._candidates;
  }

  get min(): number {
    return this._min;
  }

  get max(): number {
    return this._max;
  }

  getSummary(candidateVoteCount: CandidateVoteCount): string {
    switch (this.getClass(candidateVoteCount.count)) {
      case 'success' :
        return candidateVoteCount.candidateName.toUpperCase() + ' is winning';
      case 'danger' :
        return candidateVoteCount.candidateName.toUpperCase() + ' is losing';
      case 'default' :
        return candidateVoteCount.candidateName.toUpperCase() + ' is neither winning nor losing';
      case 'warning' :
        return 'Everybody is winning with '+ candidateVoteCount.count +' vote(s)!!';
      case 'info' :
        return 'No vote casted';
      default:
        return "";
    }
  }


  getClass(count: number): string {
    if (this.max == 0) return 'info';

    if (this.max == this.min) return 'warning';

    if (count == this.max) {
      return 'success'
    }

    if (count == this.min) {
      return 'danger'
    }

    return 'default';
  }
}
