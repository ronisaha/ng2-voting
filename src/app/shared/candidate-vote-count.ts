export class CandidateVoteCount {
  private _count: number = 0;

  constructor(private _candidateName: string) {}

  vote() {
    this._count++;
  }

  get candidateName(): string {
    return this._candidateName;
  }

  get count(): number {
    return this._count;
  }
}
