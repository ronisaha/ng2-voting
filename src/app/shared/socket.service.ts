import {Injectable, Inject} from '@angular/core';
import {DOCUMENT} from "@angular/platform-browser";

@Injectable()
export class SocketService {
  private _socket;

  constructor(@Inject(DOCUMENT) private document) {
    this._socket = io('http://' + document.location.hostname + ':8000');
  }

  get socket(){
    return this._socket;
  }
}
